## Welcome to CLI Flashcard app

This repository contains a basic flashcard app based on Laravel v9 and PHP 8.1.

Detailed explanation can be found in below of the readme regarding the todos.

## How to run:

The project is based on Laravel Sail. Here are the steps:

1- add mysql and localhost to your host file. /etc/hosts -> `127.0.0.1 localhost mysql`

2- `composer install`

3-  rename `.env.example` file as .env file.

4- `./vendor/bin/sail up -d`

5- `./vendor/bin/sail artisan migrate`

6- `./vendor/bin/sail artisan db:seed`

7- `./vendor/bin/sail artisan flashcard:interactive` or `composer run start`

After this changes you should be able to execute tests also faster via:

`php artisan test --parallel`

## Static Analysis and linting tools

Project has also phpmd, larastan and laravel pint for code styling.

Simply run:

`composer run ci`

In order to run linter and tests.

The project is using service and repository architecture. 
Since dependency injection with console is a bit tricky,
I have used a huge init method for dependency injection and 
for class inits. There were some suggested packages but
it does not seemed well integrated such as `wilderborn/partyline`
also I didn't want to bloat project with 3rd party apps.

Any comments and/or suggestions are welcome!

## CODE COVERAGE

An example code coverage report can be found at tests/reports/index.html in the repository. 
It's 87.8! Can be reproduced via `composer run coverage` or `./vendor/bin/sail composer run coverage`

## TODOS

## Tests

Tests are added for validation and main happy paths.

## Validations 

Input validations are mostly added and done.

## Larastan and PHPMD reports

Some of the warnings are coming from Laravel itself, apart from those all warnings are fixed.

## WILD Queries, Strict Typing
Some of the queries should still need to go repositories.

## Architecture 

It could be interesting to use Actions instead of Service and Repository
artchitecture but also I wanted avoid another 3rd party solution ->
`https://laravelactions.com/`

Services are only for the context of Console. It could be interesting approach
to put one more abstraction layer and refactoring services in a way that
it can be used simultaneously as a web app. But such consideration was not mentioned 
in the task pdf.

## Stats Table

Usually in gaming redis is also being considered as DB storage not just cache,
but since in the task description SQL is requested I've passed that.
Also simply used Laravel Eloquent hooks.
