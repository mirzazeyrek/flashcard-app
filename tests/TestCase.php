<?php

namespace Tests;

use App\Models\Flashcard;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\PendingCommand;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;

    public const TEST_USER = ['email' => 'mirza@studocu.com', 'password' => '123456', 'name' => 'test user'];

    public const TEST_FLASHCARD = ['question' => 'kindergarten', 'answer' => 'der'];

    public function createUserAndLogin(): PendingCommand
    {
        $this->createUser(self::TEST_USER['email'], self::TEST_USER['password'], self::TEST_USER['name']);

        return $this->login();
    }

    public function login(): PendingCommand
    {
        return $this->artisan('flashcard:interactive')
            ->expectsQuestion('Please enter your email:', self::TEST_USER['email'])
            ->expectsQuestion('Please enter your password:', self::TEST_USER['password']);
    }

    public function createUser(
        string $email = self::TEST_USER['email'],
        string $password = self::TEST_USER['password'],
        string $name = self::TEST_USER['name']
    ): User {
        return User::factory()->create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
        ]);
    }

    public function createFlashcard(int $user_id = 1, string $question = self::TEST_FLASHCARD['question'], string $answer = self::TEST_FLASHCARD['answer']): Flashcard
    {
        return Flashcard::factory()->create([
            'question' => $question,
            'answer' => $answer,
            'user_id' => $user_id,
        ]);
    }
}
