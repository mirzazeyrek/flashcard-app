<?php

namespace Tests\Feature;

use App\Console\Services\DashboardService;
use App\Console\Services\ListAllFlashcardsService;
use App\Console\Services\PracticeService;
use App\Console\Services\StatsService;
use App\Models\Flashcard;
use App\Models\Practice;
use App\Models\Stats;
use App\Models\User;
use Tests\TestCase;

class FlashcardInteractiveTest extends TestCase
{
    public function test_can_create_an_account_and_exit(): void
    {
        // no users yet.
        $this->assertEquals(0, User::count());

        $this->artisan('flashcard:interactive')
            ->expectsQuestion('Please enter your email:', parent::TEST_USER['email'])
            ->expectsQuestion('Please enter your name:', parent::TEST_USER['name'])
            ->expectsQuestion('We will create a new account with this email. Please enter your password:', parent::TEST_USER['password'])
            ->expectsQuestion('Please enter your password again:', parent::TEST_USER['password'])
            ->expectsQuestion('Please choose an option', DashboardService::MAIN_CHOICES[6])
            ->assertExitCode(0);

        // verify created user in db.
        $this->assertEquals(1, User::count());
    }

    public function test_can_login_into_existing_account(): void
    {
        // no users yet.
        $this->assertEquals(0, User::count());

        $this->createUserAndLogin()
        ->expectsQuestion('Please choose an option', DashboardService::MAIN_CHOICES[6])
        ->expectsOutput('Exit')
        ->expectsOutput('Thank you for using our app. Goodbye '.parent::TEST_USER['name'].'!')
        ->assertExitCode(0);

        // verify created user in db.
        $this->assertEquals(1, User::count());
    }

    public function test_can_create_a_flashcard(): void
    {
        // no flashcards yet.
        $this->assertEquals(0, Flashcard::count());

        $this->createUserAndLogin()
        ->expectsQuestion('Please choose an option', DashboardService::MAIN_CHOICES[1])
        ->expectsQuestion('Please enter your question:', parent::TEST_FLASHCARD['question'])
        ->expectsQuestion('Please enter your answer:', parent::TEST_FLASHCARD['answer'])
        ->expectsConfirmation('Do you wish to continue creating another question?')
        ->expectsQuestion('Please choose an option', DashboardService::MAIN_CHOICES[6])
        ->expectsOutput('Exit')
        ->expectsOutput('Thank you for using our app. Goodbye '.parent::TEST_USER['name'].'!')
        ->assertExitCode(0);

        // verify the flashcards after creation.
        $this->assertEquals(1, Flashcard::count());
        $flashcard = Flashcard::first();
        $this->assertEquals($flashcard->question, parent::TEST_FLASHCARD['question']);
        $this->assertEquals($flashcard->answer, parent::TEST_FLASHCARD['answer']);
    }

    public function test_can_practice_and_exit(): void
    {
        // verify no practice in db yet.
        $this->assertEquals(0, Practice::count());

        $user = $this->createUser();
        $flashcard = $this->createFlashcard($user->id);

        $this->login()
            ->expectsQuestion('Please choose an option', DashboardService::MAIN_CHOICES[3])
            ->expectsQuestion('Choose a question to answer. Type "0" for main menu.', $flashcard->id)
            ->expectsQuestion($flashcard->question, $flashcard->answer)
            ->expectsQuestion('Please choose an option', DashboardService::MAIN_CHOICES[6])
            ->expectsOutput('Exit')
            ->expectsOutput('Thank you for using our app. Goodbye '.parent::TEST_USER['name'].'!')
            ->assertExitCode(0);

        // confirm practice in db.
        $this->assertEquals(1, Practice::count());
        $practice = Practice::first();
        $this->assertEquals($practice->status, PracticeService::ANSWER_STATUSES[0]);
        $this->assertEquals($practice->user_id, $user->id);
        $this->assertEquals($practice->flashcard_id, $flashcard->id);
    }

    public function test_can_reset(): void
    {
        $user = $this->createUser();

        $this->login()
        ->expectsQuestion('Please choose an option', DashboardService::MAIN_CHOICES[6])
        ->expectsOutput('Exit')
        ->expectsOutput('Thank you for using our app. Goodbye '.parent::TEST_USER['name'].'!')
        ->assertExitCode(0);

        Practice::factory()->count(100)->for($user)->create();
        Stats::create(['user_id' => $user->id, 'answered' => 100, 'incorrectly_answered' => 50]);

        // before reset
        $stats = Stats::where('user_id', $user->id)->first();
        $this->assertNotEquals(0, $stats->answered);
        $this->assertNotEquals(0, $stats->correctly_answered);

        $practiceCount = Practice::where('user_id', $user->id)->count();
        $this->assertNotEquals(0, $practiceCount);

        $this->login()
            ->expectsQuestion('Please choose an option', DashboardService::MAIN_CHOICES[5])
            ->expectsConfirmation('All practice progress will be deleted. Are you sure?', 'yes')
            ->expectsQuestion('Please choose an option', DashboardService::MAIN_CHOICES[6])
            ->expectsOutput('Exit')
            ->expectsOutput('Thank you for using our app. Goodbye '.parent::TEST_USER['name'].'!')
            ->assertExitCode(0);

        // after reset
        $stats = Stats::where('user_id', $user->id)->first();
        $this->assertEquals(0, $stats->answered);
        $this->assertEquals(0, $stats->correctly_answered);

        $practiceCount = Practice::where('user_id', $user->id)->count();
        $this->assertEquals(0, $practiceCount);
    }

    public function test_can_see_stats(): void
    {
        $user = $this->createUser();

        $this->login()
            ->expectsQuestion('Please choose an option', DashboardService::MAIN_CHOICES[6])
            ->expectsOutput('Exit')
            ->expectsOutput('Thank you for using our app. Goodbye '.parent::TEST_USER['name'].'!')
            ->assertExitCode(0);

        Practice::factory()->count(100)->for($user)->create();
        $answered = Practice::where('user_id', $user->id)->count();
        $correctlyAnswered = Practice::where('user_id', $user->id)->where('status', PracticeService::ANSWER_STATUSES['0'])->count();
        $rows[] = [Flashcard::count(), $answered, $correctlyAnswered];

        $this->login()
            ->expectsQuestion('Please choose an option', DashboardService::MAIN_CHOICES[4])
            ->expectsOutput('Stats')
            ->expectsTable(StatsService::TABLE_HEADERS, $rows)
            ->expectsQuestion('Please choose an option', DashboardService::MAIN_CHOICES[6])
            ->expectsOutput('Exit')
            ->expectsOutput('Thank you for using our app. Goodbye '.parent::TEST_USER['name'].'!')
            ->assertExitCode(0);
    }

    public function test_can_do_login_input_validation(): void
    {
        $validPassword = '123456';
        $name = 'mirza zeyrek';
        $email = 'mirza@studoc.com';
        $this->artisan('flashcard:interactive')
            ->expectsQuestion('Please enter your email:', 'invalid')
            ->expectsOutput('The email must be a valid email address.')
            ->expectsQuestion('Please enter your email:', $email)
            ->expectsQuestion('Please enter your name:', 'a')
            ->expectsOutput('The name must be between 2 and 255 characters.')
            ->expectsQuestion('Please enter your name:', $name)
            ->expectsQuestion('We will create a new account with this email. Please enter your password:', '123')
            ->expectsOutput('The password must be at least 6 characters.')
            ->expectsQuestion('We will create a new account with this email. Please enter your password:', $validPassword)
            ->expectsQuestion('Please enter your password again:', $validPassword.'typo')
            ->expectsOutput('The password confirmation does not match.')
            ->expectsQuestion('Please enter your password again:', $validPassword)
            ->expectsOutput('Welcome '.$name)
            ->expectsQuestion('Please choose an option', DashboardService::MAIN_CHOICES[6])
            ->expectsOutput('Exit')
            ->expectsOutput('Thank you for using our app. Goodbye '.$name.'!')
            ->assertExitCode(0);

        $this->createUser();
        // make sure login won't allow wrong credentials for an existing user and shows a validation message.
        $this->artisan('flashcard:interactive')
            ->expectsQuestion('Please enter your email:', parent::TEST_USER['email'])
            ->expectsQuestion('Please enter your password:', 'invalidpassword')
            ->expectsOutput('Wrong credentials please check your password and try again!')
            ->expectsQuestion('Please enter your password:', parent::TEST_USER['password'])
            ->expectsOutput('Welcome '.parent::TEST_USER['name'])
            ->expectsQuestion('Please choose an option', DashboardService::MAIN_CHOICES[6])
            ->expectsOutput('Exit')
            ->expectsOutput('Thank you for using our app. Goodbye '.parent::TEST_USER['name'].'!')
            ->assertExitCode(0);
    }

    public function test_can_see_all_flashcards()
    {
        $this->artisan('db:seed');
        $rows = Flashcard::all('question', 'answer')->toArray();
        $this->createUserAndLogin()
            ->expectsQuestion('Please choose an option', DashboardService::MAIN_CHOICES[2])
            ->expectsOutput(DashboardService::MAIN_CHOICES[2])
            ->expectsTable(ListAllFlashcardsService::TABLE_HEADERS, $rows)
            ->expectsQuestion('Please choose an option', DashboardService::MAIN_CHOICES[6])
            ->expectsOutput('Exit')
            ->expectsOutput('Thank you for using our app. Goodbye '.parent::TEST_USER['name'].'!')
            ->assertExitCode(0);
    }

    public function test_can_dashboard_handle_wrong_choice_input()
    {
        $this->createUserAndLogin()
            ->expectsQuestion('Please choose an option', 'Wrong Choice')
            ->expectsOutput('Invalid option is selected. Please type a number in the list.')
            ->expectsQuestion('Please choose an option', DashboardService::MAIN_CHOICES[6])
            ->expectsOutput('Exit')
            ->expectsOutput('Thank you for using our app. Goodbye '.parent::TEST_USER['name'].'!')
            ->assertExitCode(0);
    }
}
