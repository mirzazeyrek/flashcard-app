<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flashcards', function (Blueprint $table) {
            $table->id();
            // Length limits can be adjusted according to app needs. Better not to leave without limit.
            $table->string('question', 350);
            $table->string('answer', 50);
            // Considering in the task the user deletion process is not defined I'm just going
            // with the foreign key structure. If intention is keeping the user data after user
            // account is deleted. There could be an anonymization process.
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flashcards');
    }
};
