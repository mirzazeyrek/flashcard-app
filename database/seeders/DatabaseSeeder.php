<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Flashcard;
use App\Models\Practice;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users = User::factory(10)->create();
        foreach ($users as $user) {
            if (! App::runningUnitTests()) {
                echo 'User created : '.$user->email.PHP_EOL;
            }
            Auth::loginUsingId($user->id);
            $flashcards = Flashcard::factory(rand(1, 4))->for($user)->create();
            foreach ($flashcards as $flashcard) {
                Practice::factory()->for($flashcard)->for($user)->create();
            }
        }
        if (! App::runningUnitTests()) {
            echo 'Use "password" as password and one of the created users above for login.';
        }
    }
}
