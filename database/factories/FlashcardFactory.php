<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\FlashcardFactory>
 */
class FlashcardFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $number1 = fake()->numberBetween(0, 50);
        $number2 = fake()->numberBetween(0, 50);
        $sum = $number1 + $number2;

        return [
            'question' => $number1.' + '.$number2.' = ?',
            'answer' => $sum,
            'user_id' => function () {
                return User::factory()->create()->id;
            },
        ];
    }
}
