<?php

namespace Database\Factories;

use App\Models\Flashcard;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PracticeFactory>
 */
class StatsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $flashcardCount = Flashcard::count();
        $answered = rand(0, $flashcardCount);
        $correctly_answered = rand(0, $answered);

        return [
            'answered' => $answered,
            'user_id' => User::factory()->create()->id,
            'correctly_answered' => $correctly_answered,
        ];
    }
}
