<?php

namespace Database\Factories;

use App\Console\Services\PracticeService;
use App\Models\Flashcard;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Auth;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PracticeFactory>
 */
class PracticeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'status' => PracticeService::ANSWER_STATUSES[rand(0, 1)],
            'user_id' => function () {
                $user_id = User::factory()->create()->id;
                Auth::loginUsingId($user_id);

                return $user_id;
            },
            'flashcard_id' => function () {
                return Flashcard::factory()->create()->id;
            },
        ];
    }
}
