<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Flashcard extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function practice(): HasOne
    {
        return $this->hasOne(Practice::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(Practice::class);
    }
}
