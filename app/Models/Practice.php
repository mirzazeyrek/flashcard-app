<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

class Practice extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected static function booted(): void
    {
        static::saved(function ($practice) {
            $stats = Stats::where('user_id', Auth::user()->id)->first();
            $countAnswered = $stats->answered;
            $stats->answered = ++$countAnswered;
            if ($practice->status === 'CORRECT') {
                $countCorrectlyAnswered = $stats->correctly_answered;
                $stats->correctly_answered = ++$countCorrectlyAnswered;
            }
            $stats->save();
        });
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(Practice::class);
    }

    public function flashcard(): BelongsTo
    {
        return $this->belongsTo(Flashcard::class);
    }
}
