<?php

namespace App\Repositories;

use App\Models\Flashcard;
use App\Models\Stats;

class StatsRepository
{
    protected Stats $stats;

    protected Flashcard $flashcard;

    public function __construct(Stats $stats, Flashcard $flashcard)
    {
        $this->stats = $stats;
        $this->flashcard = $flashcard;
    }

    public function resetByUserId(int $id): Stats
    {
        $stats = $this->getByUserId($id);
        $stats->answered = 0;
        $stats->correctly_answered = 0;
        $stats->save();

        return $stats->refresh();
    }

    public function getByUserId(int $id): Stats
    {
        return $this->stats->where('user_id', $id)->first();
    }
}
