<?php

namespace App\Repositories;

use App\Models\Practice;

class PracticesRepository
{
    protected Practice $practice;

    public function __construct(Practice $practice)
    {
        $this->practice = $practice;
    }

    public function deleteByUserId(int $id): bool
    {
        $delete = Practice::where('user_id', $id)->delete();

        return $delete;
    }

    public function save(int $choice, int $userId, string $status): Practice
    {
        $practice = new Practice();
        $practice->flashcard_id = $choice;
        $practice->user_id = $userId;
        // Already validated in PracticeService::getAnswerAndSaveStatus
        // @phpstan-ignore-next-line
        $practice->status = $status;
        $practice->save();

        return $practice;
    }
}
