<?php

namespace App\Repositories;

use App\Console\Services\PracticeService;
use App\Models\Flashcard;
use Illuminate\Support\Facades\Auth;

class FlashcardRepository
{
    protected Flashcard $flashcard;

    public function __construct(Flashcard $flashcard)
    {
        $this->flashcard = $flashcard;
    }

    /**
     * @return array<int, array{no: int, id: int, question: string, status: 'CORRECT'|'INCORRECT'|'NOT ANSWERED'}>
     */
    public function getStatusTable(): array
    {
        return $this->flashcard::with(['practice' => function ($query) {
            $query->select('flashcard_id', 'status')
                ->where('user_id', Auth::user()->id);
        }])->get()->map(function ($flashcard, $key) {
            // possible bug with larastan: typing property when passing closure is causing array type mismatch.
            /** @var Flashcard $flashcard */
            $flashcard = $flashcard;

            return [
                'no' => (int) $key + 1,
                'id' => $flashcard->id,
                'question' => $flashcard->question,
                'status' => $flashcard->practice?->status ?? PracticeService::ANSWER_STATUSES[2],
            ];
        })->toArray();
    }

    /**
     * @return array{correct_answers: int, unanswered_questions: array<int, int>}
     */
    public function getProgressArray(array $flashCardsWithPractice): array
    {
        $totalCorrectAnswers = 0;
        $unansweredQuestions = [];

        foreach ($flashCardsWithPractice as $key => $flashcard) {
            $status = $flashcard['status'];

            if ($status === PracticeService::ANSWER_STATUSES[0]) {
                $totalCorrectAnswers++;
            }

            if ($status === PracticeService::ANSWER_STATUSES[2]) {
                $unansweredQuestions[$key + 1] = $flashcard['id'];
            }
        }

        return ['correct_answers' => $totalCorrectAnswers, 'unanswered_questions' => $unansweredQuestions];
    }

    public function getAllQuestionsAndAnswers(): array
    {
        return $this->flashcard->get(['question', 'answer'])->toArray();
    }

    public function count(): int
    {
        return $this->flashcard->count();
    }

    public function create(string $question, string $answer, int $userId): Flashcard
    {
        $flashcard = new Flashcard();
        $flashcard->question = $question;
        $flashcard->answer = $answer;
        $flashcard->user_id = $userId;
        $flashcard->save();

        return $flashcard;
    }
}
