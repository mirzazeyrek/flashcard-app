<?php

namespace App\Console\Commands;

use App\Console\Services\CreateFlashcardService;
use App\Console\Services\DashboardService;
use App\Console\Services\ListAllFlashcardsService;
use App\Console\Services\PracticeService;
use App\Console\Services\ResetService;
use App\Console\Services\StatsService;
use App\Console\Services\UserService;
use App\Console\Services\ValidationService;
use App\Exceptions\ShutdownException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;

class FlashcardInteractive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'flashcard:interactive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Flashcard app made with Laravel + Artisan';

    public CreateFlashcardService $createFlashcardService;

    public ListAllFlashcardsService $listAllFlashcardsService;

    public UserService $userService;

    public PracticeService $practiceService;

    public ResetService $resetService;

    public StatsService $statsService;

    public DashboardService $dashboardService;

    public ValidationService $validationService;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(
        CreateFlashcardService $createFlashcardService,
        DashboardService $dashboardService,
        ListAllFlashcardsService $listAllFlashcardsService,
        PracticeService $practiceService,
        ResetService $resetService,
        StatsService $statsService,
        UserService $userService,
        ValidationService $validationService
    ): int {
        try {
            /**
             * due to command console limitations this was a bit tricky but passing the same object is a solution,
             * which is not going to create a memory issue due to how objects are being passed.
             *
             * SEE:
             * https://www.php.net/manual/en/language.oop5.references.php
             * https://www.tutorialspoint.com/php-objects-and-references
             *
             * Please kindly let me know if you have a better way to use console commands in services :)
             */
            $this->initServices(
                $createFlashcardService,
                $dashboardService,
                $listAllFlashcardsService,
                $userService,
                $practiceService,
                $resetService,
                $statsService,
                $validationService
            );

            $this->userService->handle();

            $this->greeting();
            $this->dashboardService->show();
        } catch (ShutdownException $ex) {
            // Needed for bringing execution to return 0 point.
        }

        return 0;
    }

    public function initServices(
        CreateFlashcardService $createFlashcardService,
        DashboardService $dashboardService,
        ListAllFlashcardsService $listAllFlashcardsService,
        UserService $userService,
        PracticeService $practiceService,
        ResetService $resetService,
        StatsService $statsService,
        ValidationService $validationService
    ): void {
        $this->createFlashcardService = $createFlashcardService;
        $this->createFlashcardService->init($this);

        $this->dashboardService = $dashboardService;
        $this->dashboardService->init($this);

        $this->listAllFlashcardsService = $listAllFlashcardsService;
        $this->listAllFlashcardsService->init($this);

        $this->practiceService = $practiceService;
        $this->practiceService->init($this);

        $this->resetService = $resetService;
        $this->resetService->init($this);

        $this->statsService = $statsService;
        $this->statsService->init($this);

        $this->userService = $userService;
        $this->userService->init($this);

        $this->validationService = $validationService;
    }

    public function greeting(): void
    {
        $this->info('Welcome '.Auth::user()->name);
    }

    public function showProgressBar(int $total, int $progress): void
    {
        $progressbar = $this->output->createProgressBar($total);
        $progressbar->setProgress(0);
        for ($i = 0; $i < 1; $i++) {
            sleep(1);
            $progressbar->advance($progress);
        }
        $this->newLine(1);
    }
}
