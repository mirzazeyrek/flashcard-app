<?php

namespace App\Console\Services;

use App\Models\Stats;
use App\Repositories\FlashcardRepository;
use App\Repositories\StatsRepository;
use Illuminate\Support\Facades\Auth;

class StatsService extends DashboardService
{
    public StatsRepository $statsRepository;

    public FlashcardRepository $flashcardRepository;

    public const TABLE_HEADERS = ['Questions', 'Answered', 'Correct Answers'];

    public function __construct(StatsRepository $statsRepository, FlashcardRepository $flashcardRepository)
    {
        $this->statsRepository = $statsRepository;
        $this->flashcardRepository = $flashcardRepository;
    }

    public function getByUserId(int $id): Stats
    {
        return $this->statsRepository->getByUserId($id);
    }

    public function show(): void
    {
        $rows = [];
        $stats = $this->getByUserId(Auth::user()->id);
        $rows[] = [$this->flashcardRepository->count(), $stats->answered, $stats->correctly_answered];
        $this->command->table(self::TABLE_HEADERS, $rows);
        $this->command->dashboardService->show();
    }
}
