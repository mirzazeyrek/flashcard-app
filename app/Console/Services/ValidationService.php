<?php

namespace App\Console\Services;

use Illuminate\Support\Facades\Validator;

class ValidationService extends DashboardService
{
    public Validator $validator;

    public string $validationFailedMessage;

    public function __construct(Validator $validator)
    {
        $this->validator = $validator;
    }

    public function validateEmail(?string $input): bool
    {
        $validator = $this->validator::make(['email' => $input], [
            'email' => 'email:rfc',
        ]);

        if ($validator->fails()) {
            $this->validationFailedMessage = $validator->errors()->first('email');

            return false;
        }

        return true;
    }

    public function validateName(?string $input): bool
    {
        $validator = $this->validator::make(['name' => $input], [
            'name' => 'required|between:2,255',
        ]);

        if ($validator->fails()) {
            $this->validationFailedMessage = $validator->errors()->first('name');

            return false;
        }

        return true;
    }

    public function validatePassword(?string $input): bool
    {
        $validator = $this->validator::make(['password' => $input], [
            'password' => [
                'required',
                'string',
                'min:6',              // must be at least 6 characters in length
                'max:50',             // must be maximum 50 characters in length
                // Example rules for increasing password security:
                //'regex:/[a-z]/',      // must contain at least one lowercase letter
                //'regex:/[A-Z]/',      // must contain at least one uppercase letter
                //'regex:/[0-9]/',      // must contain at least one digit
                //'regex:/[@$!%*#?&]/', // must contain a special character
            ],
        ]);

        if ($validator->fails()) {
            $this->validationFailedMessage = $validator->errors()->first('password');

            return false;
        }

        return true;
    }

    public function validatePasswordConfirmation(?string $input, ?string $password): bool
    {
        $validator = $this->validator::make(['password_confirmation' => $input, 'password' => $password], [
            'password' => 'confirmed',
        ]);

        if ($validator->fails()) {
            $this->validationFailedMessage = $validator->errors()->first('password');

            return false;
        }

        return true;
    }

    public function validateQuestion(?string $input): bool
    {
        $validator = $this->validator::make(['question' => $input], [
            'question' => 'required|between:1,350',
        ]);

        if ($validator->fails()) {
            $this->validationFailedMessage = $validator->errors()->first('question');

            return false;
        }

        return true;
    }

    public function validateAnswer(?string $input): bool
    {
        $validator = $this->validator::make(['answer' => $input], [
            'answer' => 'required|between:1,50',
        ]);

        if ($validator->fails()) {
            $this->validationFailedMessage = $validator->errors()->first('answer');

            return false;
        }

        return true;
    }
}
