<?php

namespace App\Console\Services;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserService extends DashboardService
{
    public function login(?User $user, string $email): void
    {
        while ($user !== null && ! ($this->loginUser($user, $email))) {
        }
    }

    public function loginUser(User $user, string $email): ?User
    {
        while (! $password = $this->promptPassword(false)) {
            $this->command->error($this->command->validationService->validationFailedMessage);
        }

        $auth = Auth::attempt(['email' => $email, 'password' => $password]);
        if (! $auth) {
            $this->command->error('Wrong credentials please check your password and try again!');

            return null;
        }
        Auth::loginUsingId($user->id);

        return $user;
    }

    public function createUser(string $email): ?User
    {
        while (! $name = $this->promptName()) {
            $this->command->error($this->command->validationService->validationFailedMessage);
        }

        while (! $password = $this->promptPassword(true)) {
            $this->command->error($this->command->validationService->validationFailedMessage);
        }

        while (! $this->promptPasswordConfirmation($password)) {
            $this->command->error($this->command->validationService->validationFailedMessage);
        }

        $user = new User();
        $user->password = Hash::make($password);
        $user->email = $email;
        $user->name = $name;
        $user->save();
        Auth::loginUsingId($user->id);

        return $user;
    }

    public function getUser(string $email): ?User
    {
        return User::whereEmail($email)->first();
    }

    public function handle(): void
    {
        while (! $email = $this->promptEmail()) {
            $this->command->error($this->command->validationService->validationFailedMessage);
        }

        $user = $this->getUser($email);
        // create a user if not exists.
        if (! $user) {
            $this->createUser($email);
        }
        // login if user is exists.
        $this->login($user, $email);
    }

    public function promptEmail(): ?string
    {
        $input = $this->command->ask('Please enter your email:');
        if ($this->command->validationService->validateEmail($input)) {
            return $input;
        }

        return null;
    }

    public function promptName(): ?string
    {
        $input = $this->command->ask('Please enter your name:');
        if ($this->command->validationService->validateName($input)) {
            return $input;
        }

        return null;
    }

    public function promptPassword(bool $isNewUser): ?string
    {
        $this->showPasswordInputTip();

        $inputMessage = 'Please enter your password:';

        if ($isNewUser) {
            $inputMessage = 'We will create a new account with this email. '.$inputMessage;
        }

        $input = $this->command->secret($inputMessage);
        if ($this->command->validationService->validatePassword($input)) {
            return $input;
        }

        return null;
    }

    public function promptPasswordConfirmation(string $password): ?string
    {
        $input = $this->command->secret('Please enter your password again:');
        if ($this->command->validationService->validatePasswordConfirmation($input, $password)) {
            return $input;
        }

        return null;
    }

    public function showPasswordInputTip(): void
    {
        $this->command->info('TIP: Password input won\'t be shown due to security.');
    }
}
