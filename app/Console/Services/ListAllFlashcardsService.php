<?php

namespace App\Console\Services;

use App\Repositories\FlashcardRepository;

class ListAllFlashcardsService extends DashboardService
{
    public FlashcardRepository $flashcardRepository;

    public const TABLE_HEADERS = ['Questions', 'Answers'];

    public function __construct(FlashcardRepository $flashcardRepository)
    {
        $this->flashcardRepository = $flashcardRepository;
    }

    public function show(): void
    {
        $this->command->table(
            self::TABLE_HEADERS,
            $this->flashcardRepository->getAllQuestionsAndAnswers()
        );

        parent::show();
    }
}
