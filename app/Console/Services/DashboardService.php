<?php

namespace App\Console\Services;

use App\Console\Commands\FlashcardInteractive;
use App\Exceptions\ShutdownException;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Auth;

class DashboardService
{
    public FlashcardInteractive $command;

    // TODO: decouple dashboard logic into a FlashcardDashboardRepository.
    public const MAIN_CHOICES = [
        1 => 'Create a Flashcard',
        2 => 'List all Flashcards',
        3 => 'Practice',
        4 => 'Stats',
        5 => 'Reset',
        6 => 'Exit',
    ];

    public function init(FlashcardInteractive $command): void
    {
        $this->command = $command;
    }

    public function show(): void
    {
        while (! $this->chooseAnOption()) {
        }
    }

    public function chooseAnOption(): bool
    {
        /** @var string $optionChosen * */
        $optionChosen = $this->command->choice('Please choose an option', self::MAIN_CHOICES);
        $this->command->info($optionChosen);

        return $this->validateChosenOption($optionChosen);
    }

    /**
     * @throws ShutdownException
     */
    public function validateChosenOption(string $option): bool
    {
        $valid = true;
        switch ($option) {
            case self::MAIN_CHOICES[1]:
                $this->command->createFlashcardService->show();
                break;
            case self::MAIN_CHOICES[2]:
                $this->command->listAllFlashcardsService->show();
                break;
            case self::MAIN_CHOICES[3]:
                $this->command->practiceService->show();
                break;
            case self::MAIN_CHOICES[4]:
                $this->command->statsService->show();
                break;
            case self::MAIN_CHOICES[5]:
                $this->command->resetService->show();
                break;
            case self::MAIN_CHOICES[6]:
                $this->optionExit();
                break;
            default:
                $this->command->error('Invalid option is selected. Please type a number in the list.');
                $valid = false;
        }

        return $valid;
    }

    public function optionExit(): void
    {
        $this->command->info('Thank you for using our app. Goodbye '.Auth::user()->name.'!');
        $this->command->info(Inspiring::quote());
        throw new ShutdownException;
    }
}
