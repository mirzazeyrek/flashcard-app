<?php

namespace App\Console\Services;

use App\Models\Flashcard;
use App\Repositories\FlashcardRepository;
use App\Repositories\PracticesRepository;
use Illuminate\Support\Facades\Auth;

class PracticeService extends DashboardService
{
    public const ANSWER_STATUSES = [
        0 => 'CORRECT',
        1 => 'INCORRECT',
        2 => 'NOT ANSWERED',
    ];

    public PracticesRepository $practicesRepository;

    public FlashcardRepository $flashcardRepository;

    public function __construct(PracticesRepository $practicesRepository, FlashcardRepository $flashcardRepository)
    {
        $this->practicesRepository = $practicesRepository;
        $this->flashcardRepository = $flashcardRepository;
    }

    public function show(): void
    {
        $this->optionPractice();
    }

    public function optionPractice(): void
    {
        $this->showAddQuestionsMessage();

        $statusTable = $this->flashcardRepository->getStatusTable();
        $progressArr = $this->flashcardRepository->getProgressArray($statusTable);

        $this->showQuestionsTable($statusTable);

        $this->showProgressBar(count($statusTable), $progressArr['correct_answers']);

        $this->goBackToDashboardWhenAllAnswered(count($progressArr['unanswered_questions']));

        $choice = $this->getChoices($progressArr['unanswered_questions']);

        $flashcard = $this->getChosenFlashcard($choice);
        $this->getAnswerAndSaveStatus($flashcard, $choice);

        // go back to main menu
        $this->optionPractice();
    }

    public function getChosenFlashcard(int $choice): Flashcard
    {
        $flashcard = Flashcard::where('id', $choice)->first();
        if (! $flashcard) {
            $this->command->error('wrong id');
        }

        return $flashcard;
    }

    public function getAnswerAndSaveStatus(Flashcard $flashcard, int $choice): void
    {
        $answer = trim(mb_strtolower($this->command->ask($flashcard->question)));
        $status = self::ANSWER_STATUSES[1];
        if ($answer === trim(mb_strtolower($flashcard->answer))) {
            $status = self::ANSWER_STATUSES[0];
        }
        $this->practicesRepository->save($choice, Auth::user()->id, $status);
    }

    public function showAddQuestionsMessage(): void
    {
        $exists = Flashcard::first();
        if ($exists === null) {
            $this->command->info('Please add flashcards 1st for practising!');
            $this->command->dashboardService->show();
        }
    }

    public function showProgressBar(int $totalQuestions, int $totalCorrectAnswers): void
    {
        $this->command->showProgressBar($totalQuestions, $totalCorrectAnswers);
        $this->command->info($totalCorrectAnswers.' correct answers out of '.$totalQuestions.'.');
        if ($totalQuestions === $totalCorrectAnswers) {
            $this->command->info('Impressive! All correct.');
        }
    }

    public function goBackToDashboardWhenAllAnswered(int $unansweredQuestions): void
    {
        if ($unansweredQuestions === 0) {
            $this->command->info('No unanswered questions left. You can reset your progress to start over.');
            $this->command->dashboardService->show();
        }
    }

    public function showQuestionsTable(array $questionsAndStatusesArr): void
    {
        $this->command->table(['No', 'Id', 'Question', 'Answer Status'], $questionsAndStatusesArr);
    }

    public function getChoices(array $unansweredQuestions): int
    {
        $choice = (int) $this->command->choice('Choose a question to answer. Type "0" for main menu.', $unansweredQuestions + [0 => 0]);
        if ($choice === 0) {
            $this->command->dashboardService->show();
        }

        return $choice;
    }
}
