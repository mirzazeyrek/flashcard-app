<?php

namespace App\Console\Services;

use Illuminate\Support\Facades\Auth;

class ResetService extends DashboardService
{
    public function resetByUserId(int $id): bool
    {
        $this->command->practiceService->practicesRepository->deleteByUserId($id);
        $this->command->statsService->statsRepository->resetByUserId($id);

        return true;
    }

    public function show(): void
    {
        if ($this->command->confirm('All practice progress will be deleted. Are you sure?')) {
            $this->resetByUserId(Auth::user()->id);
        }
        parent::show();
    }
}
