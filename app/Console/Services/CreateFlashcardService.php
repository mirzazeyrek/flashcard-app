<?php

namespace App\Console\Services;

use App\Repositories\FlashcardRepository;
use Illuminate\Support\Facades\Auth;

class CreateFlashcardService extends DashboardService
{
    public FlashcardRepository $flashcardRepository;

    public function __construct(FlashcardRepository $flashcardRepository)
    {
        $this->flashcardRepository = $flashcardRepository;
    }

    public function show(): void
    {
        while (! $question = $this->promptQuestion()) {
            $this->command->error($this->command->validationService->validationFailedMessage);
        }

        while (! $answer = $this->promptAnswer()) {
            $this->command->error($this->command->validationService->validationFailedMessage);
        }

        $this->flashcardRepository->create($question, $answer, Auth::user()->id);
        $this->command->info('Your flashcard is created.');
        if ($this->command->confirm('Do you wish to continue creating another question?')) {
            $this->show();
        }
        // go back to main menu
        $this->command->dashboardService->show();
    }

    public function promptQuestion(): ?string
    {
        $input = $this->command->ask('Please enter your question:');
        if ($this->command->validationService->validateQuestion($input)) {
            return $input;
        }

        return null;
    }

    public function promptAnswer(): ?string
    {
        $input = $this->command->ask('Please enter your answer:');
        if ($this->command->validationService->validateAnswer($input)) {
            return $input;
        }

        return null;
    }
}
